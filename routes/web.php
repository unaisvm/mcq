<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return view('auth.login');
});

Route::get('/', function () {
    return view('welcome');
});
Route::get('srtep-1', function () {
    return view('step1');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('add-student', 'ExamController@addStudent');

Route::get('/exam', 'ExamController@questions');

Route::post('submit-answer', 'ExamController@answerAdd');

Route::get('/questions/answer', 'ExamController@answer');

Route::get('/students', 'HomeController@students');






