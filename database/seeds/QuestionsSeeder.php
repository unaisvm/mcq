<?php

use Illuminate\Database\Seeder;
use App\Question;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::truncate();

        $questions =  [
            [
              'question' => 'What is part of a database that holds only one type of information?',
              'option1' => 'Report',
              'option2' => 'Field',
              'option3' => 'Record',
              'option4' => 'File',
              'answer' => 'Field',
            ],
            [
                'question' => 'OS computer abbreviation usually means ?',
                'option1' => 'Order of Significance',
                'option2' => 'Open Software',
                'option3' => 'Operating System',
                'option4' => 'Optical Sensor',
                'answer' => 'Operating System',
            ],
            [
                'question' => '.MOV extension refers usually to what kind of file?',
                'option1' => 'Image file',
                'option2' => 'Animation/movie file',
                'option3' => 'Audio file',
                'option4' => 'MS Office document',
                'answer' => 'Animation/movie file',
            ],
            [
                'question' => 'What frequency range is the High Frequency band?',
              'option1' => '100 kHz',
              'option2' => '1 GHz',
              'option3' => '30 to 300 MHz',
              'option4' => '3 to 30 MHz',
              'answer' => '3 to 30 MHz',
              ],
              [
                'question' => 'What do you call a computer on a network that requests files from another computer?',
              'option1' => 'A client',
              'option2' => 'A host',
              'option3' => 'A router',
              'option4' => 'A web serve',
              'answer' => 'A client',
              ],
              [
                'question' => 'Hardware devices that are not part of the main computer system and are often added later to the system.',
              'option1' => 'Peripheral',
              'option2' => 'Clip art',
              'option3' => 'Highlight',
              'option4' => 'Execute',
              'answer' => 'Peripheral',
              ],
              [
                'question' => 'The main computer that stores the files that can be sent to computers that are networked together is:',
              'option1' => 'Clip art',
              'option2' => 'Mother board',
              'option3' => 'Peripheral',
              'option4' => 'File server',
              'answer' => 'File server',
              ],
              [
                'question' => 'How can you catch a computer virus?',
              'option1' => 'Sending e-mail messages',
              'option2' => 'Using a laptop during the winter',
              'option3' => 'Opening e-mail attachments',
              'option4' => 'Shopping on-line',
              'answer' => 'Opening e-mail attachments',
              ],
              [
                'question' => 'Google (www.google.com) is a:',
              'option1' => 'Search Engine',
              'option2' => 'Number in Math',
              'option3' => 'Directory of images',
              'option4' => 'Chat service on the web',
              'answer' => 'Search Engine',
              ],
                [
                    'question' => 'Which is not an Internet protocol?',
                    'option1' => 'HTTP',
                    'option2' => 'FTP',
                    'option3' => 'STP',
                    'option4' => 'IP',
                    'answer' => 'STP',
                ]
          ];

          Question::insert($questions);
    
    }
}
