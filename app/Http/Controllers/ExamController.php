<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Student;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Session;
class ExamController extends Controller
{
   public function addStudent(Request $request){
      
   // return $request;
    $slug = Str::random(25);
    $data=new Student();
    $data->name=$request->name;
    $data->phone=$request->phone;
    $data->slug=$slug;
    $data->save();
    session()->put('user', $data->id);
    return Redirect::to(url('/exam'));
   }

   public function questions(){

      $questions=Question::get()->take(10);
     
      if(session('user')){

         //return $questions;
         return view('questions',compact('questions'));

      }
      else{
         return Redirect::to(url('srtep-1'));

      }
   }

   public function answerAdd(Request $request){
      //return $request;
      if(session('user')){
      $result=0;
      $user=session('user');
      $answer=Question::pluck('answer')->take(10);
       
      for($i=0;$i<10;$i++){
         if($i!=0){
            $j=$i-1;
         }
         else{
            $j=$i;
         }
         
         if($request->input('qst'.$i)===$answer[$j]){
          //return 1;
          $result= $result += 1;
         }
         
        
      }
      
    //  return $request->input('qst'.$i);
     // return $answer[9];
      $student=Student::find($user);
      $student->mark=$result;
      $student->update();
      session()->put('answer', $result);
      return Redirect::to(url('/questions/answer'));

   }
   else{
      return Redirect::to(url('srtep-1'));

   }
      
   }

   public function answer(){

      $questions=Question::get()->take(10);
     
      if(session('user')){
         session()->forget('user');
         //return $questions;
         return view('answer',compact('questions'));

      }
      else{
         return Redirect::to(url('srtep-1'));

      }
   }

}
