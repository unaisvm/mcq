<!DOCTYPE html>
<html lang="en">
<head>
  <title>MCQ</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container-fluid p-5 bg-light text-black text-center">
  <h1>Multiple Choice Questions</h1>
  <p>Enter Details</p>
</div>

<div class="container mt-5">
  <form class="row g-3" method="POST" action="{{url('add-student')}}">
    @csrf
    <div class="col-md-6">
      <label for="inputEmail4" class="form-label">Name</label>
      <input type="text" class="form-control" id="inputEmail4" name="name" required>
    </div>
    <div class="col-md-6">
      <label for="inputPassword4" class="form-label">Phone</label>
      <input type="number" class="form-control"  id="inputPassword4" name="phone" required>
    </div>
   
    <div class="col-6" style="float: right;">
    </div>

    <div class="col-6" style="float: right;">
     <a href="/"><button type="button" class="btn btn-primary ">Back</button></a> 

      <button type="submit" class="btn btn-success ">Next</button>
    </div>
  </form>
</div>

</body>
</html>
