<!DOCTYPE html>
<html lang="en">
<head>
  <title>MCQ</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <style>
.choices {
  list-style-type: none;
  padding: 0;
}

.choices li {
  margin-bottom: 5px;
}

.choices label {
  display: flex;
  align-items: center;
}

.choices label,
input[type="radio"] {
  cursor: pointer;
}

input[type="radio"] {
  margin-right: 8px;
}

  </style>
</head>
<body>

<div class="container-fluid p-5 bg-secondary text-white text-center">
  <h1>Multiple Choice Question & Answer</h1>
  <a href="{{url('srtep-1')}}" class="btn btn-primary btn-lg"> Try again</a>

</div>
  
<div class="container mt-5">
 
    @foreach($questions as $question)
    <div class="col-12"> 
      <h5>{{$loop->iteration}}.{{$question->question}}</h5>
      <p>Ans: {{$question->answer}}</p>
    </div>
    @endforeach
   
  
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  @if (session('answer'))
  swal("Your Score is {{ session('answer') }}/10","","success");
  @endif
</script>

</body>
</html>
