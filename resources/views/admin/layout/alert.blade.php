@if (session('success'))
<div class="alert alert-success border-0 alert-dismissible" id="alert">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    <span class="font-weight-semibold">{{ session('success') }}!</span>
</div>
@endif

@if (session('error'))
<div class="alert alert-danger border-0 alert-dismissible" id="alert">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    <span class="font-weight-semibold">{{ session('error') }}!</span>
</div>
@endif
<script>
    setTimeout(function() {
        $('#alert').fadeOut('slow');
    }, 2000); // <-- time in milliseconds
</script>
