@extends('admin.layout.main')

@section('content')

<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">

        <div class="ecommerce-widget">



            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-header" >
                            
                            <h5 >Students</h5>

                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Contact</th>
                                            <th>Mark</th>
                                            

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($students as $student )
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $student->name }}</td>
                                            <td>{{ $student->phone }}</td>
                                            <td>{{$student->mark}}/10</td>
                                            

                                        </tr>
                                        @endforeach

                                    </tbody>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>









@endsection
