<!DOCTYPE html>
<html lang="en">
<head>
  <title>MCQ</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <style>
.choices {
  list-style-type: none;
  padding: 0;
}

.choices li {
  margin-bottom: 5px;
}

.choices label {
  display: flex;
  align-items: center;
}

.choices label,
input[type="radio"] {
  cursor: pointer;
}

input[type="radio"] {
  margin-right: 8px;
}

  </style>
</head>
<body>

<div class="container-fluid p-5 bg-secondary text-white text-center">
  <h1>Multiple Choice Questions</h1>

</div>
  
<div class="container mt-5">
  <form class="row g-3" method="POST" action="{{url('submit-answer')}}">
    @csrf
    @foreach($questions as $question)
    <div class="col-12"> 
      <h5>{{$loop->iteration}}.{{$question->question}}</h5>
      <ul class="choices">
        <li><label><input type="radio" name="qst{{$question->id}}" value="{{$question->option1}}" required><span>{{$question->option1}}</span></label></li>
        <li><label><input type="radio" name="qst{{$question->id}}" value="{{$question->option2}}" required><span>{{$question->option2}}</span></label></li>
        <li><label><input type="radio" name="qst{{$question->id}}" value="{{$question->option3}}" required><span>{{$question->option3}}</span></label></li>
        <li><label><input type="radio" name="qst{{$question->id}}" value="{{$question->option4}}" required><span>{{$question->option4}}</span></label></li>
      </ul>
    </div>
    @endforeach
    <div class="col-12">
      <button type="submit"  class="btn btn-primary">Submit</button>
         </div>
    
  </form>
</div>



</body>
</html>
